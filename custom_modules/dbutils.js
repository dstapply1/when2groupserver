var sqlite3 = require('sqlite3');
var crypto = require('crypto');

var db = new sqlite3.Database('./database/database.sqlite3');

// Helper functions

function hashPassword(password, salt) {
  var hash = crypto.createHash('sha256');
  hash.update(password);
  hash.update(salt);
  return hash.digest('hex');
}

// DB functions

exports.init = function() {
  db.serialize(function() {
    db.get("SELECT id FROM users", function(err, row) {
      if (err) {
        db.run('CREATE TABLE users ('
          + 'id INTEGER PRIMARY KEY AUTOINCREMENT,'
          + 'username TEXT NOT NULL,'
          + 'password TEXT NOT NULL,'
          + 'salt INTEGER,'
          + 'first_name TEXT,'
          + 'last_name TEXT,'
          + 'schedule TEXT)'
        );
        db.run('CREATE TABLE meetings ('
          + 'id INTEGER PRIMARY KEY AUTOINCREMENT,'
          + 'groupId INTEGER NOT NULL,'
          + 'title TEXT NOT NULL,'
          + 'agenda TEXT,'
          + 'creator INTEGER,'
          + 'schedule TEXT,'
          + 'latitude TEXT,'
          + 'longitude TEXT,'
          + 'FOREIGN KEY (groupId) REFERENCES groups(id),'
          + 'FOREIGN KEY (creator) REFERENCES users(id))'
        );
        db.run('CREATE TABLE groups ('
          + 'id INTEGER PRIMARY KEY AUTOINCREMENT,'
          + 'creator INTEGER NOT NULL,'
          + 'name TEXT NOT NULL,'
          + 'schedule TEXT,'
          + 'FOREIGN KEY (creator) REFERENCES users(id))'
        );
        db.run('CREATE TABLE group_members ('
          + 'userId INTEGER,'
          + 'groupId INTEGER,'
          + 'status TEXT,'
          + 'PRIMARY KEY (userId, groupId),'
          + 'FOREIGN KEY (userId) REFERENCES users(id),'
          + 'FOREIGN KEY (groupId) REFERENCES groups(id))'
        );
        console.log("db created");
      } else {
        console.log("db initialized");
      }
    });
  });
}

// returns user id which is > 0, or -1 on failure
exports.login = function (username, password, callback) {
  db.get('SELECT salt FROM users WHERE username = ?', username.trim().toLowerCase(), function(err, row) {
    if (!row) {
      console.log("User with username: " + username + ", does not exist.");
      return callback(-1);
    }
    var hash = hashPassword(password, row.salt);
    db.get('SELECT username, id FROM users WHERE username = ? AND password = ?', username.trim().toLowerCase(), hash, function(err, row) {
      if (!row) {
        console.log("User with username: " + username + ", attempted to login with incorrect password.");
        return callback(-1);
      }
      return callback(row.id);
    });
  });
}

// returns id on success, -1 on failure
exports.register = function(username, password, callback) {
  db.get('SELECT id, username FROM users WHERE username = ?', username.trim().toLowerCase(), function(err, row) {
    if (err) {
      console.log('Error in Registration: ' + err);
      return callback(-1);
    }
    if (row) {
      console.log('User already exists');
      return callback(-1);
    } else {
      // our salt is creation timestamp
      var salt = (new Date()).toString();
      var hash = hashPassword(password, salt);

      db.run("INSERT INTO users (username, password, salt, first_name, last_name, schedule) VALUES (?, ?, ?, '', '', '')", username.trim().toLowerCase(), hash, salt, function(err) {
        if (err) {
          console.log('Error in Saving user: ' + err);
          return callback(-1);
        }
        console.log('User Registration successful');
        db.get('SELECT id, username FROM users WHERE username = ?', username.trim().toLowerCase(), function(err, row) {
          if (err) {
            console.log('Error getting user after registration');
            return callback(-1);
          }
          if (row) {
            console.log("User Registration complete")
            return callback(row.id);
          }
          return callback(-1);
        });
      });
    }
  });
}

exports.getUserInfo = function(userId, callback) {
  db.get('SELECT username, first_name, last_name, schedule FROM users WHERE id = ?', userId, function(err, row) {
    if (err) {
      console.log('Error getting info for userId: ' + userId);
      return callback(null);
    }
    if (row) {
      return callback(row.username, row.first_name, row.last_name, row.schedule);
    }
    console.log("No info for userId: " + userId);
    return callback(null);
  });
}

exports.updateUserInfo = function(userId, firstName, lastName, schedule, callback) {
  db.run("UPDATE users SET first_name = ?, last_name = ?, schedule = ? WHERE id = ?", firstName, lastName, schedule, userId, function(err) {
    if (err) {
      console.log("Error updating info for user: " + userId);
      return callback(-1);
    }
    // return userId on success
    return callback(userId);
  });
}

exports.getUserSchedule = function(userId, callback) {
  db.get('SELECT schedule FROM users WHERE id = ?', userId, function(err, row) {
    if (err) {
      console.log('Error getting schedule for userId: ' + userId);
      return callback(null);
    }
    if (row) {
      return callback(row.schedule);
    }
    console.log("No schedule for userId: " + userId);
    return callback(null);
  });
}

// return userId on success, -1 on failure
exports.updateUserSchedule = function(userId, schedule, callback) {
  db.run("UPDATE users SET schedule = ? WHERE id = ?", schedule, userId, function(err) {
    if (err) {
      console.log("Error updating schedule for user: " + userId);
      return callback(-1);
    }
    // return userId on success
    return callback(userId);
  });
}

exports.createMeeting = function(groupId, title, agenda, creator, schedule, latitude, longitude, callback) {
  db.run("INSERT INTO meetings (groupId, title, agenda, creator, schedule, latitude, longitude) VALUES (?, ?, ?, ?, ?, ?, ?)", groupId, title, agenda, creator, schedule, latitude, longitude, function(err) {
    if (err) {
      console.log('Error in Saving meeting: ' + err);
      return callback(-1);
    }
    console.log('Meeting creation successful');
    db.get('SELECT id, groupId, title FROM meetings WHERE groupId = ? AND title = ? ORDER BY id DESC LIMIT 1', groupId, title, function(err, row) {
      if (err) {
        console.log('Error getting meeting after creation');
        return callback(-1);
      }
      if (row) {
        console.log("Meeting creation complete");
        return callback(row.id);
      }
      console.log("No meeting with specified info");
      return callback(-1);
    });
  });
}

exports.getMeeting = function(meetingId, callback) {
  db.get('SELECT groupId, title, agenda, creator, schedule, latitude, longitude FROM meetings WHERE id = ?', meetingId, function(err, row) {
    if (err) {
      console.log('Error getting meeting for meetingId: ' + meetingId);
      return callback(null);
    }
    if (row) {
      return callback(row.groupId, row.title, row.agenda, row.creator, row.schedule, row.latitude, row.longitude);
    }
    console.log("No meeting for meetingId: " + meetingId);
    return callback(null);
  });
}

exports.createGroup = function(creator, name, schedule, callback) {
  db.run("INSERT INTO groups (creator, name, schedule) VALUES (?, ?, ?)", creator, name, schedule, function(err) {
    if (err) {
      console.log('Error in Saving group: ' + err);
      return callback(-1);
    }
    console.log('Group creation successful');
    db.get('SELECT id FROM groups WHERE name = ? ORDER BY id DESC LIMIT 1', name, function(err, row) {
      if (err) {
        console.log('Error getting group after creation');
        return callback(-1);
      }
      if (row) {
        console.log("Group creation complete");
        return callback(row.id);
      }
      console.log("No group with specified info");
      return callback(-1);
    });
  });
}

exports.getGroup = function(groupId, callback) {
  db.get('SELECT creator, name, schedule FROM groups WHERE id = ?', groupId, function(err, row) {
    if (err) {
      console.log('Error getting group for groupId: ' + groupId);
      return callback(null);
    }
    if (row) {
      return callback(row.creator, row.name, row.schedule);
    }
    console.log("No group for groupId: " + groupId);
    return callback(null);
  });
}

exports.getUserGroups = function(userId, callback) {
  db.all('SELECT groupId, status FROM group_members WHERE userId = ? ORDER BY groupId DESC', userId, function(err, rows) {
    if (err) {
      console.log('Error getting groups for userId: ' + userId);
      return callback(null);
    }
    if (rows != undefined && rows.length > 0) {
      return callback(rows);
    }
    console.log("No groups for userId: " + userId);
    return callback(null);
  });
}

exports.addGroupMember = function(userId, groupId, status, callback) {
  db.run("REPLACE INTO group_members(userId, groupId, status) VALUES (?, ?, ?)", userId, groupId, status, function(err) {
    if (err) {
      console.log('Error in Saving group_member: ' + err);
      return callback(-1);
    }
    console.log('Group_member added successfully');
    return callback(1);
  });
}

exports.getUserId = function(email, callback) {
  db.get('SELECT id FROM users WHERE username = ?', email.trim().toLowerCase(), function(err, row) {
    if (err) {
      console.log('Error getting userId for email: ' + email);
      return callback(-1);
    }
    if (row) {
      return callback(row.id);
    } else {
      console.log("No userId for email: " + email);
    }
    return callback(-1);
  });
}

exports.getGroupMeetings = function(groupId, callback) {
  db.all('SELECT id, title, agenda, creator, schedule, latitude, longitude FROM meetings WHERE groupId = ? ORDER BY id DESC', groupId, function(err, rows) {
    if (err) {
      console.log('Error getting meeings for groupId: ' + groupId);
      return callback(null);
    }
    if (rows != undefined && rows.length > 0) {
      return callback(rows);
    }
    console.log("No meetings for groupId: " + groupId);
    return callback(null);
  });
}

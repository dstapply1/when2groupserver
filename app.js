
var url = require('url');
var express = require('express');
var expressSession = require('express-session');
var path = require('path');
var fs = require('fs');
var bodyParser = require('body-parser');
var dbUtils = require('./custom_modules/dbutils');

var enableLogging = true;

var app = express();
var port = process.env.PORT || 3000;

// configure
app.use(express.static(__dirname + '/public'));
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());
// initialize db
dbUtils.init();

// Helper functions

function parseStringArray(stringArray) {
  var strippedStringArray = stringArray.substring(1, stringArray.length-1);
  var array = strippedStringArray.split(",");
  var newArray = [];
  for (var i = 0; i < array.length; i++) {
    var item = array[i].trim();
    newArray.push(item);
  }
  return newArray;
}

function parseSchedule(stringSched) {
  // parse sched from string
  var sched = JSON.parse("[" + stringSched + "]");

  return sched;
}

function scheduleToString(schedule) {
  var stringSched = "";
  for (var i = 0; i < 7; i++) {
    stringSched += "[";
    stringSched += schedule[i].toString();
    stringSched += "],";
  }
  stringSched = stringSched.substring(0, stringSched.length-1);

  return stringSched;
}

// GET POST

app.get("/", function(req, res) {
  res.send("When2Group Server");
});

app.get("/test", function(req, res) {

  console.log("Testing......")

  /** Test Schedules **/
  var testSchedule = [[0,0,0,0,0,0,0,1,1,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,1,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0],
                      [0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0]];
  var stringSched = scheduleToString(testSchedule);
  console.log(stringSched);
  var parsedSched = parseSchedule(stringSched);
  console.log(parsedSched[0][6]);
  console.log(parsedSched[0][7]);

  /** Test Login/Registration **/
  var uname = "testuser";
  var pass = "password123";

  dbUtils.register(uname, pass, function(id) {
    if (id > 0) {
      console.log("Test User Registration successful.");

      dbUtils.login(uname, pass, function(id) {
        if (id > 0) {
          console.log("Test User Login successful.");
        } else {
          console.log("Test User Login failed.");
        }
      });
    } else {
      console.log("Test User Registration failed.");
    }
  });

  console.log("Tests complete.");

  res.send("Tests Complete.");
});

app.post("/register", function(req, res) {
  var username = req.body.username;
  var password = req.body.password;
  dbUtils.register(username, password, function(id) {
    if (id > 0) {
      console.log("User: " + username + ", registered successfully");
      var result = { result: "success", userId: id };
      res.send(JSON.stringify(result));
    } else {
      console.log("User: " + username + ", registration failed");
      var result = { result: "failure" };
      res.send(JSON.stringify(result));
    }
  });
});

app.post("/auth", function(req, res) {
  var username = req.body.username;
  var password = req.body.password;
  dbUtils.login(username, password, function(id) {
    if (id > 0) {
      console.log("User: " + username + ", authenticated successfully");
      var result = { result: "success", userId: id };
      res.send(JSON.stringify(result));
    } else {
      console.log("User: " + username + ", authentication failed");
      var result = { result: "failure" };
      res.send(JSON.stringify(result));
    }
  });
});

app.post("/getUserInfo", function(req, res) {
  var userId = req.body.userId;

  if (userId != undefined && userId > 0) {
    dbUtils.getUserInfo(userId, function(username, firstName, lastName, schedule) {
      console.log(username + ", " + firstName + ", " + lastName + ", " + schedule);
      var result = {};
      if (schedule != null) {
        result = { result: "success", userId: userId, username: username, firstName: firstName, lastName: lastName, schedule: schedule };
      } else {
        result = { result: "failure" };
      }
      res.send(JSON.stringify(result));
    });
  } else {
    var result = { result: "failure" };
    res.send(JSON.stringify(result));
  }
});

app.post("/updateUserInfo", function(req, res) {
  var userId = req.body.userId;
  var firstName = req.body.firstName;
  var lastName = req.body.lastName;
  var schedule = req.body.schedule;

  console.log(firstName + ", " + lastName + ", " + schedule);

  if (userId != undefined && userId > 0) {
    dbUtils.updateUserInfo(userId, firstName, lastName, schedule, function(id) {
      var result = {};
      if (id > 0) {
        result = { result: "success", userId: userId };
      } else {
        result = { result: "failure" };
      }
      res.send(JSON.stringify(result));
    });
  } else {
    var result = { result: "failure" };
    res.send(JSON.stringify(result));
  }
});

app.post("/getUserSchedule", function(req, res) {
  var userId = req.body.userId;

  if (userId != undefined && userId > 0) {
    dbUtils.getUserSchedule(userId, function(schedule) {
      console.log(schedule);
      var result = {};
      if (schedule != null) {
        result = { result: "success", userId: userId, schedule: schedule };
      } else {
        result = { result: "failure" };
      }
      res.send(JSON.stringify(result));
    });
  } else {
    var result = { result: "failure" };
    res.send(JSON.stringify(result));
  }
});

app.post("/updateUserSchedule", function(req, res) {
  var userId = req.body.userId;
  var schedule = req.body.schedule;

  console.log(schedule);

  if (userId != undefined && userId > 0) {
    dbUtils.updateUserSchedule(userId, schedule, function(id) {
      var result = {};
      if (id > 0) {
        result = { result: "success", userId: userId };
      } else {
        result = { result: "failure" };
      }
      res.send(JSON.stringify(result));
    });
  } else {
    var result = { result: "failure" };
    res.send(JSON.stringify(result));
  }
});

app.post("/createMeeting", function(req, res) {
  var groupId = req.body.groupId;
  var title = req.body.title;
  var agenda = req.body.agenda;
  var creator = req.body.creator;
  var schedule = req.body.schedule;
  var latitude = req.body.latitude;
  var longitude = req.body.longitude;

  if (groupId != undefined && groupId > 0) {
    dbUtils.createMeeting(groupId, title, agenda, creator, schedule, latitude, longitude, function(meetingId) {
      var result = {};
      if (meetingId > 0) {
        result = { result: "success", meetingId: meetingId };
      } else {
        result = { result: "failure" };
      }
      res.send(JSON.stringify(result));
    });
  } else {
    var result = { result: "failure" };
    res.send(JSON.stringify(result));
  }
});

app.post("/getMeeting", function(req, res) {
  var meetingId = req.body.meetingId;

  if (meetingId != undefined && meetingId > 0) {
    dbUtils.getMeeting(meetingId, function(groupId, title, agenda, creator, schedule, latitude, longitude) {
      var result = {};
      if (groupId != null) {
        result = { result: "success", groupId: groupId, title: title, agenda: agenda, creator: creator, schedule: schedule, latitude: latitude, longitude: longitude };
      } else {
        result = { result: "failure" };
      }
      res.send(JSON.stringify(result));
    });
  } else {
    var result = { result: "failure" };
    res.send(JSON.stringify(result));
  }
});

app.post("/createGroup", function(req, res) {
  var creator = req.body.creator;
  var name = req.body.name;
  var schedule = req.body.schedule;

  if (creator != undefined && creator > 0) {
    dbUtils.createGroup(creator, name, schedule, function(groupId) {
      var result = {};
      if (groupId > 0) {
        result = { result: "success", groupId: groupId };
      } else {
        result = { result: "failure" };
      }
      res.send(JSON.stringify(result));
    });
  } else {
    var result = { result: "failure" };
    res.send(JSON.stringify(result));
  }
});

app.post("/addGroupMember", function(req, res) {
  var email = req.body.email;
  var groupId = req.body.groupId;
  var status = req.body.status;

  console.log(email);

  if (groupId != undefined && groupId > 0) {
    dbUtils.getUserId(email, function(userId) {
      dbUtils.addGroupMember(userId, groupId, status, function(val) {
        var result = {};
        if (val > 0) {
          result = { result: "success" };
          console.log("Successfully addedGroupMember.");
        } else {
          result = { result: "failure" };
        }
        res.send(JSON.stringify(result));
      });
    });
  } else {
    var result = { result: "failure" };
    res.send(JSON.stringify(result));
  }
});

app.post("/getGroup", function(req, res) {
  var groupId = req.body.groupId;

  if (groupId != undefined && groupId > 0) {
    dbUtils.getGroup(groupId, function(creator, name, schedule) {
      var result = {};
      if (creator != null) {
        result = { result: "success", creator: creator, name: name, schedule: schedule };
      } else {
        result = { result: "failure" };
      }
      res.send(JSON.stringify(result));
    });
  } else {
    var result = { result: "failure" };
    res.send(JSON.stringify(result));
  }
});

app.post("/getUserGroups", function(req, res) {
  var userId = req.body.userId;

  if (userId != undefined && userId > 0) {
    dbUtils.getUserGroups(userId, function(rows) {
      var result = {};
      if (rows != null) {
        result = { result: "success", groups: JSON.stringify(rows) };
      } else {
        result = { result: "failure" };
      }
      res.send(JSON.stringify(result));
    });
  } else {
    var result = { result: "failure" };
    res.send(JSON.stringify(result));
  }
});

app.post("/getGroupMeetings", function(req, res) {
  var groupId = req.body.groupId;

  if (groupId != undefined && groupId > 0) {
    dbUtils.getGroupMeetings(groupId, function(rows) {
      var result = {};
      if (rows != null) {
        result = { result: "success", meetings: JSON.stringify(rows) };
      } else {
        result = { result: "failure" };
      }
      res.send(JSON.stringify(result));
    });
  } else {
    var result = { result: "failure" };
    res.send(JSON.stringify(result));
  }
});

app.post("/getGroup", function(req, res) {
  var groupId = req.body.groupId;

  if (groupId != undefined && groupId > 0) {
    dbUtils.getGroup(groupId, function(creator, name, schedule) {
      var result = {};
      if (creator != null && creator > 0) {
        result = { result: "success", creator: creator, name: name, schedule: schedule };
      } else {
        result = { result: "failure" };
      }
      res.send(JSON.stringify(result));
    });
  } else {
    var result = { result: "failure" };
    res.send(JSON.stringify(result));
  }
});

app.post("/test", function(req, res) {
  console.log(req.body);

  var json = { server: "stuff" };
  res.send(JSON.stringify(json));
});

app.listen(port, function() {
  console.log('App is listening on port ' + port);
});
